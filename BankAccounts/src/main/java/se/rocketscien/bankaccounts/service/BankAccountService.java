package se.rocketscien.bankaccounts.service;

import se.rocketscien.bankaccounts.entity.Account;

import java.math.BigDecimal;

public interface BankAccountService {

    Account createAccount();

    Account getAccount(Long accountId);

    void transfer(Long fromAccount, Long toAccount, BigDecimal amount);

    Account refill(Long accountId, BigDecimal amount);

    Account withdrawal(Long accountId, BigDecimal amount);

}
