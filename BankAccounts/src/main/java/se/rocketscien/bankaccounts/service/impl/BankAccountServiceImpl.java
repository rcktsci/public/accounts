package se.rocketscien.bankaccounts.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.rocketscien.bankaccounts.entity.Account;
import se.rocketscien.bankaccounts.exception.AccountNotFoundException;
import se.rocketscien.bankaccounts.exception.AmountIsNotPositiveException;
import se.rocketscien.bankaccounts.exception.NotEnoughMoneyForOperationException;
import se.rocketscien.bankaccounts.repository.AccountRepository;
import se.rocketscien.bankaccounts.service.BankAccountService;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {

    private final AccountRepository accountRepository;

    @Override
    public Account createAccount() {
        return accountRepository.save(Account.builder().amount(BigDecimal.ZERO).build());
    }

    @Override
    public Account getAccount(Long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    @Override
    @Transactional
    public void transfer(Long fromAccount, Long toAccount, BigDecimal amount) {
        checkAmountIsPositive(amount);
        Account from;
        Account to;
        if (toAccount < fromAccount) {//Select accounts for update in order of id to avoid of deadlock
            to = getAccountForUpdate(toAccount);
            from = getAccountForUpdate(fromAccount);
        } else {
            from = getAccountForUpdate(fromAccount);
            to = getAccountForUpdate(toAccount);
        }
        checkIfWithdrawalCorrect(from, amount);
        from.setAmount(from.getAmount().subtract(amount));
        to.setAmount(to.getAmount().add(amount));
        accountRepository.save(from);
        accountRepository.save(to);
    }

    @Override
    @Transactional
    public Account refill(Long accountId, BigDecimal amount) {
        checkAmountIsPositive(amount);
        Account account = getAccountForUpdate(accountId);
        account.setAmount(account.getAmount().add(amount));
        return accountRepository.save(account);
    }

    @Override
    @Transactional
    public Account withdrawal(Long accountId, BigDecimal amount) {
        checkAmountIsPositive(amount);
        Account account = getAccountForUpdate(accountId);
        checkIfWithdrawalCorrect(account, amount);
        account.setAmount(account.getAmount().subtract(amount));
        return accountRepository.save(account);
    }

    private void checkAmountIsPositive(BigDecimal amount) {
        if (amount.signum() < 1) {
            throw new AmountIsNotPositiveException();
        }
    }

    private Account getAccountForUpdate(Long accountId) {
        return accountRepository.getForUpdate(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    private void checkIfWithdrawalCorrect(Account account, BigDecimal amount) {
        if (account.getAmount().subtract(amount).signum() == -1 ) {
            throw new NotEnoughMoneyForOperationException(account.getId(), amount);
        }
    }
}
