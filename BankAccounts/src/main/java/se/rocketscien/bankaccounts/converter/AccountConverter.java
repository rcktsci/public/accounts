package se.rocketscien.bankaccounts.converter;

import org.mapstruct.Mapper;
import se.rocketscien.bankaccounts.dto.AccountDto;
import se.rocketscien.bankaccounts.entity.Account;

@Mapper
public interface AccountConverter {

    AccountDto toDto(Account account);
}
