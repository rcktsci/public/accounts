package se.rocketscien.bankaccounts.repository;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import se.rocketscien.bankaccounts.entity.Account;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Query(value = "SELECT a.* FROM account AS a WHERE a.id = :id FOR UPDATE", nativeQuery = true)
    Optional<Account> getForUpdate(@Param("id") Long id);
}
