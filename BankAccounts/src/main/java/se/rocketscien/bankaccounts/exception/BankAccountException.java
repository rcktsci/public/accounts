package se.rocketscien.bankaccounts.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@AllArgsConstructor
public class BankAccountException extends RuntimeException{

    private String errorCode;

    private List<Object> params;

    private HttpStatus httpStatus;
}
