package se.rocketscien.bankaccounts.exception;

import org.springframework.http.HttpStatus;

import java.util.Collections;

public class AmountIsNotPositiveException extends BankAccountException {

    public AmountIsNotPositiveException() {
        super("amount.negative", Collections.emptyList(), HttpStatus.BAD_REQUEST);
    }
}
