package se.rocketscien.bankaccounts.exception;

import org.springframework.http.HttpStatus;
import java.math.BigDecimal;
import java.util.Arrays;

public class NotEnoughMoneyForOperationException extends BankAccountException {

    public NotEnoughMoneyForOperationException(Long accountId, BigDecimal amount) {
        super("account.not.enough.money", Arrays.asList(accountId, amount), HttpStatus.BAD_REQUEST);
    }
}
