package se.rocketscien.bankaccounts.exception;

import org.springframework.http.HttpStatus;

import java.util.Collections;

public class AccountNotFoundException extends BankAccountException {

    public AccountNotFoundException(Long accountId) {
        super("account.not.found", Collections.singletonList(accountId), HttpStatus.NOT_FOUND);
    }
}
