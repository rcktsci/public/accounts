package se.rocketscien.bankaccounts.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("Account info")
public class AccountDto {

    @ApiModelProperty("Unique id of account")
    private Long id;

    @ApiModelProperty("Amount of money of account")
    private BigDecimal amount;
}
