package se.rocketscien.bankaccounts.api;

import lombok.Value;

@Value
public class ErrorData {

    String code;

    String message;
}
