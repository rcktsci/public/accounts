package se.rocketscien.bankaccounts.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import se.rocketscien.bankaccounts.converter.AccountConverter;
import se.rocketscien.bankaccounts.dto.AccountDto;
import se.rocketscien.bankaccounts.service.BankAccountService;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
@Api("API to manage bank account")
public class AccountController {

    private final AccountConverter accountConverter;

    private final BankAccountService bankAccountService;

    @GetMapping("/{accountId}")
    @ApiOperation("Get info about account")
    public AccountDto getAccount(@ApiParam("ID of account") @PathVariable("accountId") Long accountId) {
        return accountConverter.toDto(bankAccountService.getAccount(accountId));
    }

    @PostMapping
    @ApiOperation("Create new account and get it info")
    public AccountDto create() {
        return accountConverter.toDto(bankAccountService.createAccount());
    }

    @PostMapping("/{accountId}/refill")
    @ApiOperation("Refill account and get it info")
    public AccountDto refill(
            @ApiParam("ID of account") @PathVariable("accountId") Long accountId,
            @ApiParam("Amount to refill") @RequestBody BigDecimal amount
    ) {
        return accountConverter.toDto(bankAccountService.refill(accountId, amount));
    }

    @PostMapping("/{accountId}/withdrawal")
    @ApiOperation("Withdrawal from account and get it info")
    public AccountDto withdrawal(
            @ApiParam("ID of account") @PathVariable("accountId") Long accountId,
            @ApiParam("Amount for withdrawal") @RequestBody BigDecimal amount
    ) {
        return accountConverter.toDto(bankAccountService.withdrawal(accountId, amount));
    }

    @PostMapping("/{from}/transfer/{to}")
    @ApiOperation("Transfer money from one account to another")
    public void withdrawal(
            @ApiParam("ID of account from transfer") @PathVariable("from") Long fromId,
            @ApiParam("ID of account to transfer") @PathVariable("to") Long toId,
            @ApiParam("Amount to transfer") @RequestBody BigDecimal amount
    ) {
        bankAccountService.transfer(fromId, toId, amount);
    }
}
