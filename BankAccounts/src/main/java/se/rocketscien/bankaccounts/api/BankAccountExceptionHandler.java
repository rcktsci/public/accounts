package se.rocketscien.bankaccounts.api;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import se.rocketscien.bankaccounts.exception.BankAccountException;

import java.util.Locale;

@RestControllerAdvice
@RequiredArgsConstructor
public class BankAccountExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(BankAccountExceptionHandler.class);

    private final MessageSource messageSource;

    @ExceptionHandler(value = BankAccountException.class)
    public ResponseEntity<ErrorData> handleWarehouseException(BankAccountException ex) {
        logger.error(ex.getErrorCode(), ex);
        return new ResponseEntity<>(
                new ErrorData(ex.getErrorCode(),
                        messageSource.getMessage(ex.getErrorCode(), ex.getParams().toArray(), Locale.getDefault())
                ),
                new HttpHeaders(), ex.getHttpStatus());
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorData> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorData("method.argument.mismatch",
                messageSource.getMessage("method.argument.mismatch", new String[]{ex.getName()}, Locale.getDefault())),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<ErrorData> handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorData("method.param.mismatch",
                messageSource.getMessage("method.param.mismatch", new String[]{ex.getParameterName()}, Locale.getDefault())),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorData> handleException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorData("unknown.exception", ex.getMessage()),
                new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
