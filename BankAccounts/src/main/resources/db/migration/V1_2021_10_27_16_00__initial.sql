CREATE TABLE account(
    id SERIAL NOT NULL CONSTRAINT account_id_pk PRIMARY KEY,
    amount DECIMAL
);