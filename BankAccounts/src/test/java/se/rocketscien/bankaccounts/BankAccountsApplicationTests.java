package se.rocketscien.bankaccounts;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.shaded.org.apache.commons.io.IOUtils;
import se.rocketscien.bankaccounts.dto.AccountDto;
import se.rocketscien.bankaccounts.repository.AccountRepository;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(value = "classpath:before.sql")
@Sql(value = "classpath:after.sql", executionPhase = AFTER_TEST_METHOD)
class BankAccountsApplicationTests {

    protected static final String JSON_PATH = "src/test/resources/json/";

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private AccountRepository accountRepository;

    @LocalServerPort
    private int port;

    protected String host;

    @BeforeEach
    void before() {
        host = "http://localhost:" + port;
    }

    @Test
    public void create() {
        ResponseEntity<AccountDto> response = testRestTemplate.postForEntity(host + "/api/account/","", AccountDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getAmount(), is(BigDecimal.ZERO));
    }

    @ParameterizedTest
    @MethodSource("getArgumentsForGet")
    public void get(String id, HttpStatus status, String responseBody) throws JSONException {
        ResponseEntity<String> response = testRestTemplate.getForEntity(host + "/api/account/" + id, String.class);

        assertThat(response.getStatusCode(), is(status));
        JSONAssert.assertEquals(responseBody, response.getBody(), false);
    }

    private static Stream<Arguments> getArgumentsForGet() throws IOException {
        return Stream.of(
                Arguments.of("1", HttpStatus.OK, String.format(getJson("account1"), "1000")),
                Arguments.of(Long.MAX_VALUE + "", HttpStatus.NOT_FOUND, getJson("account-not-found")),
                Arguments.of("StringIdentifer", HttpStatus.BAD_REQUEST, getJson("string-identifier"))
        );
    }

    @ParameterizedTest
    @MethodSource("getArgumentsForRefill")
    public void refill(String id, BigDecimal amount, HttpStatus status, String responseData) throws JSONException {
        ResponseEntity<String> response = testRestTemplate.postForEntity(host + "/api/account/" + id + "/refill",
                amount, String.class);

        assertThat(response.getStatusCode(), is(status));
        JSONAssert.assertEquals(responseData, response.getBody(), false);
    }

    private static Stream<Arguments> getArgumentsForRefill() throws IOException {
        return Stream.of(
                Arguments.of("1", BigDecimal.valueOf(100.20), HttpStatus.OK, String.format(getJson("account1"), "1100.20")),
                Arguments.of("1", BigDecimal.valueOf(1), HttpStatus.OK, String.format(getJson("account1"), "1001")),
                Arguments.of("1", BigDecimal.valueOf(-100.20), HttpStatus.BAD_REQUEST, getJson("negative-amount")),
                Arguments.of(Long.MAX_VALUE + "", BigDecimal.valueOf(100), HttpStatus.NOT_FOUND, getJson("account-not-found")),
                Arguments.of("StringIdentifer", BigDecimal.valueOf(100), HttpStatus.BAD_REQUEST, getJson("string-identifier"))
        );
    }

    @ParameterizedTest
    @MethodSource("getArgumentsForWithdrawal")
    public void withdrawal(String id, BigDecimal amount, HttpStatus status, String responseData) throws JSONException {
        ResponseEntity<String> response = testRestTemplate.postForEntity(host + "/api/account/" + id + "/withdrawal",
                amount, String.class);

        assertThat(response.getStatusCode(), is(status));
        JSONAssert.assertEquals(responseData, response.getBody(), false);
    }

    private static Stream<Arguments> getArgumentsForWithdrawal() throws IOException {
        return Stream.of(
                Arguments.of("1", BigDecimal.valueOf(100.20), HttpStatus.OK, String.format(getJson("account1"), "899.80")),
                Arguments.of("1", BigDecimal.valueOf(1), HttpStatus.OK, String.format(getJson("account1"), "999")),
                Arguments.of("1", BigDecimal.valueOf(-100.20), HttpStatus.BAD_REQUEST, getJson("negative-amount")),
                Arguments.of("1", BigDecimal.valueOf(1001), HttpStatus.BAD_REQUEST, getJson("not-enough-money")),
                Arguments.of(Long.MAX_VALUE + "", BigDecimal.valueOf(100), HttpStatus.NOT_FOUND, getJson("account-not-found")),
                Arguments.of("StringIdentifer", BigDecimal.valueOf(100), HttpStatus.BAD_REQUEST, getJson("string-identifier"))
        );
    }

    @ParameterizedTest
    @MethodSource("getArgumentsForTransferFailed")
    public void transferFailed(String from, String to, BigDecimal amount, HttpStatus status, String responseData) throws JSONException {
        ResponseEntity<String> response = testRestTemplate.postForEntity(host + "/api/account/" + from + "/transfer/" + to,
                amount, String.class);

        assertThat(response.getStatusCode(), is(status));
        JSONAssert.assertEquals(responseData, response.getBody(), false);
    }

    private static Stream<Arguments> getArgumentsForTransferFailed() throws IOException {
        return Stream.of(
                Arguments.of("1", "2", BigDecimal.valueOf(-100.20), HttpStatus.BAD_REQUEST, getJson("negative-amount")),
                Arguments.of("1", "2", BigDecimal.valueOf(1001), HttpStatus.BAD_REQUEST, getJson("not-enough-money")),
                Arguments.of(Long.MAX_VALUE + "", "2", BigDecimal.valueOf(100), HttpStatus.NOT_FOUND, getJson("account-not-found")),
                Arguments.of("StringIdentifer", "2", BigDecimal.valueOf(100), HttpStatus.BAD_REQUEST, getJson("string-identifier")),
                Arguments.of("1", Long.MAX_VALUE + "", BigDecimal.valueOf(100), HttpStatus.NOT_FOUND, getJson("account-not-found")),
                Arguments.of("1", "StringIdentifer", BigDecimal.valueOf(100), HttpStatus.BAD_REQUEST, getJson("string-identifier"))
        );
    }

    @ParameterizedTest
    @MethodSource("getArgumentsForTransferSuccess")
    public void transferSuccess(Long from, Long to, BigDecimal amount, String fromAmount, String toAmount) throws JSONException {
        ResponseEntity<String> response = testRestTemplate.postForEntity(host + "/api/account/" + from + "/transfer/" + to,
                amount, String.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));

        assertThat(accountRepository.findById(from).get().getAmount().toString(), is(fromAmount));
        assertThat(accountRepository.findById(to).get().getAmount().toString(), is(toAmount));
    }

    private static Stream<Arguments> getArgumentsForTransferSuccess() throws IOException {
        return Stream.of(
                Arguments.of(1L, 2L, BigDecimal.valueOf(100), "900.00", "1100.00"),
                Arguments.of(2L, 1L, BigDecimal.valueOf(100), "900.00", "1100.00"),
                Arguments.of(1L, 2L, BigDecimal.valueOf(100.50), "899.50", "1100.50")
        );
    }

    private static String getJson(String fileName) throws IOException {
        FileInputStream fis = new FileInputStream(JSON_PATH + fileName + ".json");
        return IOUtils.toString(fis, "UTF-8");
    }
}
